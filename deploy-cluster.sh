#!/bin/bash
GREEN='\033[0;32m'
LB='\033[1;34m' # light blue
YL='\033[1;33m' # yellow
RD='\033[1;31m' # yellow
NC='\033[0m' # No Color

K3SVERSION=v1.21.3+k3s1 # Check latest at https://github.com/k3s-io/k3s/releases

USER=ubuntu
# RPROXY_IP="192.168.1.140"
SERVER_IP="192.168.1.141"
SEC_SERVERS_IP=()
AGENTS_IP=()
INSTALL_RANCHER=true
INSTALL_PROXY=false
PREFIX="k8s"

NUMSERVERS=$((${#SEC_SERVERS_IP[@]}+1))
NUMAGENTS=${#AGENTS_IP[@]}

SERVERS=$(eval 'echo '${PREFIX}'-server-{1..'"$((NUMSERVERS))"'}')
AGENTS=$(eval 'echo '${PREFIX}'-agent-{1..'"$((NUMAGENTS))"'}')

# check if k3sup installed
if ! command -v k3sup &> /dev/null
then
    echo -e "[${RD}ERROR${NC}] k3sup needs to be installed on the system. See: https://github.com/alexellis/k3sup"
    exit
fi

# check if kubectl installed
if ! command -v kubectl &> /dev/null
then
    echo -e "[${RD}ERROR${NC}] kubectl needs to be installed on the system. Use:  sudo snap install kubectl --classic"
    exit
fi

# check if helm installed
if ! command -v helm &> /dev/null
then
    echo -e "[${RD}ERROR${NC}] helm needs to be installed on the system. Use:  sudo snap install helm --classic"
    exit
fi

# check if multipass exists
if ! test -f "cloud-config.yaml"; then
    echo -e "[${RD}ERROR${NC}] expects the existence of the cloud-config.yaml file with your RSA pub key"
    exit
fi

# check if RSA key in multipass
if ! grep -q ssh_authorized_keys: "cloud-config.yaml"; then
    echo -e "[${RD}ERROR${NC}] RSA pub key is not in cloud-config.yaml"
    exit
fi

echo -e "[${LB}INFO${NC}] main server IP: ${SERVER_IP}"
echo -e "[${LB}INFO${NC}] secondary servers IPs: ${SEC_SERVERS_IP}"
echo -e "[${LB}INFO${NC}] agents IPs: ${AGENTS_IP}"

if [ $NUMSERVERS -gt 1 ]; then
    echo -e "[${LB}INFO${NC}] deploying HA cluster"
    echo -e "[${LB}INFO${NC}] installing k3s on server"
    k3sup install --ip $SERVER_IP --user $USER --k3s-version ${K3SVERSION} --cluster --k3s-extra-args '--no-deploy traefik'
    echo -e "[${LB}INFO${NC}] installing k3s on secondary servers"
    for SEC_SERVER_IP in $SEC_SERVERS_IP
    do
        k3sup join --ip $SEC_SERVER_IP --user $USER --server-ip $SERVER_IP --server-user $USER --server --k3s-version ${K3SVERSION} --k3s-extra-args '--no-deploy traefik'
    done
else
    echo -e "[${LB}INFO${NC}] deploying single node cluster"
    k3sup install --ip $SERVER_IP --user $USER --k3s-version ${K3SVERSION} --k3s-extra-args '--no-deploy traefik'
fi

echo -e "[${LB}INFO${NC}] setting environment variables for kubectl"
export KUBECONFIG=${PWD}/kubeconfig
kubectl config set-context default

echo -e "[${LB}INFO${NC}] waiting for server nodes to get ready"
for SERVER in ${SERVERS}
do
    while : ; do
        kubectl get node $SERVER &> /dev/null && break
        sleep 5
    done
    kubectl wait --for=condition=ready node/$SERVER --timeout=600s
done

echo -e "[${LB}INFO${NC}] joining agents to server"
for AGENT_IP in $AGENTS_IP
do
    k3sup join --ip $AGENT_IP --user $USER --server-ip $SERVER_IP --server-user $USER --k3s-version ${K3SVERSION} --k3s-extra-args '--no-deploy traefik'
done

if [ $NUMAGENTS -gt 1 ]; then
    echo -e "[${LB}INFO${NC}] waiting for agent nodes to get ready"
    for AGENT in ${AGENTS}
    do
        while : ; do
            kubectl get node $AGENT &> /dev/null && break
            sleep 5
        done
        kubectl wait --for=condition=ready node/$AGENT --timeout=600s
    done
fi
kubectl get nodes

# install nginx
kubectl create namespace ingress-nginx
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx --namespace ingress-nginx ingress-nginx/ingress-nginx

echo -e "[${LB}INFO${NC}] wait for nginx to be up and running"
kubectl wait --namespace ingress-nginx \
    --for=condition=ready pod \
    --selector=app.kubernetes.io/component=controller \
    --timeout=120s

kubectl get pods --namespace ingress-nginx

if INSTALL_PROXY=true; then
    # add entries to nginx.conf if multi server
    # otherwise bypass load balancer (point to node IP direclty in router)
    if [ $NUMSERVERS -gt 1 ]; then
        echo -e "[${LB}INFO${NC}] setting up nginx load balancer"
        mkdir -p nginx
        cp -f ./_nginx.conf ./nginx/nginx.conf
        for SEC_SERVER_IP in $SEC_SERVERS_IP
        do
            cp ./nginx/nginx.conf ./nginx/nginx_temp.conf
            awk -v SEC_SERVER_IP=$SEC_SERVER_IP '/upstream k8s_api_plane_http {/ { print; print "\t\tserver " SEC_SERVER_IP ":80;"; next }1' ./nginx/nginx_temp.conf > ./nginx/nginx.conf
            cp ./nginx/nginx.conf ./nginx/nginx_temp.conf
            awk -v SEC_SERVER_IP=$SEC_SERVER_IP '/upstream k8s_api_plane_https {/ { print; print "\t\tserver " SEC_SERVER_IP ":443;"; next }1' ./nginx/nginx_temp.conf > ./nginx/nginx.conf
        done
        cp ./nginx/nginx.conf ./nginx/nginx_temp.conf
        awk -v SERVER_IP=$SERVER_IP '/upstream k8s_api_plane_http {/ { print; print "\t\tserver " SERVER_IP ":80;"; next }1' ./nginx/nginx_temp.conf > ./nginx/nginx.conf
        cp ./nginx/nginx.conf ./nginx/nginx_temp.conf
        awk -v SERVER_IP=$SERVER_IP '/upstream k8s_api_plane_https {/ { print; print "\t\tserver " SERVER_IP ":443;"; next }1' ./nginx/nginx_temp.conf > ./nginx/nginx.conf
        rm -rf ./nginx/nginx_temp.conf

        scp nginx/nginx.conf root@$RPROXY_IP:/etc/nginx/nginx.conf
        ssh root@$RPROXY_IP '/etc/init.d/nginx restart'
    fi
fi

echo -e "[${LB}INFO${NC}] install cert manager"
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v1.0.4/cert-manager.crds.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
if [ $(($NUMAGENTS + $NUMSERVERS)) -ge 3 ]; then
    helm install \
    cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --version v1.0.4 \
    --set replicas=3
else
    helm install \
    cert-manager jetstack/cert-manager \
    --namespace cert-manager \
    --version v1.0.4 \
    --set replicas=1
fi

echo -e "[${LB}INFO${NC}] wait for cert manager to be up and running"
CERT_MANAGER_PODS=$(echo $(kubectl get pods --namespace cert-manager | awk 'NR!=1 {print $1}'))
for CERT_MANAGER_POD in ${CERT_MANAGER_PODS}
do
    kubectl wait --for=condition=ready pod/${CERT_MANAGER_POD} -n cert-manager --timeout=60s
done
kubectl get pods --namespace cert-manager

if INSTALL_RANCHER == true; then
    echo -e "[${LB}INFO${NC}] install rancher"
    kubectl create namespace cattle-system
    helm repo add rancher-latest https://releases.rancher.com/server-charts/latest
    if [ $(($NUMAGENTS + $NUMSERVERS)) -ge 3 ]; then
        helm install rancher rancher-latest/rancher \
        --namespace cattle-system \
        --set hostname=rancher.MYDOMAIN \
        --set ingress.tls.source=letsEncrypt \
        --set letsEncrypt.email=MYEMAIL \
        --set replicas=3
    else
        helm install rancher rancher-latest/rancher \
        --namespace cattle-system \
        --set hostname=rancher.MYDOMAIN \
        --set ingress.tls.source=letsEncrypt \
        --set letsEncrypt.email=MYEMAIL \
        --set replicas=1
    fi
    kubectl -n cattle-system rollout status deploy/rancher
fi

export KUBECONFIG=${PWD}/kubeconfig
kubectl config set-context default

echo -e "[${LB}INFO${NC}] create homeserver namespace"
kubectl create namespace homeserver

echo -e "[${GREEN}FINISHED${NC}]"
