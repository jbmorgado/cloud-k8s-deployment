#!/bin/bash
GREEN='\033[0;32m'
LB='\033[1;34m' # light blue
NC='\033[0m' # No Color

SERVERS=$(echo $(multipass list | grep server | awk '{print $1}'))
AGENTS=$(echo $(multipass list | grep agent | awk '{print $1}'))
NODES+=$SERVERS
NODES+=' '
NODES+=$AGENTS
NODES_A=($NODES) # easier way to check size of string is to convert to an array

if [ ${#NODES_A[@]} -ge 1 ]
then
    echo -e "[${LB}Info${NC}] found nodes: ${NODES}"
    echo -e "[${LB}Info${NC}] stop ${NODES}"
    multipass stop ${NODES}
    sleep 5
    echo -e "[${LB}Info${NC}] delete ${NODES}"
    multipass delete ${NODES}
    sleep 5
    echo -e "[${LB}Info${NC}] purge"
    multipass purge
else
    echo -e "[${LB}Info${NC}] no nodes found"
fi

echo -e "[${LB}Info${NC}] stoping and destroying nginx load balancer"
docker-compose stop;docker-compose rm -f
rm -rf ./nginx

echo -e "[${GREEN}FINISHED${NC}]"