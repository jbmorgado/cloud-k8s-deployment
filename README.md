Nota:
- instalar `sudo apt install nfs-kernel-server` nos nodes (parte da cloud-init image)

Get the repo ready by using `sanitize.sh` and prepare it for sumbission with `unsanitize.sh`

`sanitize.sh`
```
find . -type f ! -name '*sanitize.sh' -not -path "./.git/*" -exec sed -i '' 's/yourrealdomain.com/SOMEDOMAIN/g' '{}' \;
find . -type f ! -name '*sanitize.sh' -not -path "./.git/*" -exec sed -i '' 's/yourrealemail@gmail.com/SOMEEMAIL/g' '{}' \;
```

`unsanitize.sh`
```
find . -type f ! -name '*sanitize.sh' -not -path "./.git/*" -exec sed -i '' 's/SOM	EDOMAIN/yourrealdomain.com/g' '{}' \;
find . -type f ! -name '*sanitize.sh' -not -path "./.git/*" -exec sed -i '' 's/SOMEEMAIL/yourrealemail@gmail.com/g' '{}' \;
```





# Run with:
`source deploy-cluster.sh`